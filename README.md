# Godot State Machine Plugin

This is a small FSM(Finite State Machine) plugin written in GDScript for the Godot Engine version 4.x.
The plugin uses a StateMachine node, with State Resources in it, keeping your Scene Tree cleaner.

Tested with Godot Engine 4.1.


## Usage

Here is a very minimal example, without logic, on how it looks like using the plugin.

Every state machine has it's own `shared_data`, which is a Dictionary that holds data that can be used across all of it's states for easy information sharing.

You can see a working example in the project under the `src` folder. And you can test it out by running the project.

If you do not like setting it up via code, you can add the states for the StateMachine using the Inspector.

```swift
func _ready() -> void:
	var state_machine := StateMachine.new([
		WalkingState.new("Walking"),
		FlyingState.new("Flying"),
	])

	state_machine.shared_data["player"] = $%"Player"

	add_child(state_machine)


class WalkingState extends State:
	pass


class FlyingState extends State:
	pass
```


## Installation

Copy the `addons` folder into your project's root folder, and enable the plugin in `Project Settings > Plugins`.


## License

The project is licensed under the MIT License.
