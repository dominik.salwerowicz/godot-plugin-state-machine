class_name StateMachine extends Node
## Finite State Machine(FSM)
##
## Used for holding different [State]s, and giving the ability to those to change to other [State]s, while each state providing different solutions.[br]
## [br]
## Wikipedia page: [url]https://wikipedia.org/wiki/Finite-state_machine[url]


# ----------
# Properties
# ----------

## The name of the [State] it starts using when it's ready.
## If not set, it will use the first [State] at the start.
@export var start_state_name: StringName = "":
	set(value):
		start_state_name = value
		if _current_state == null and has_state(value):
			_current_state = get_state(value)

@export var _states: Array[State] = []

## A dictionary for holding information between the states.
@export var shared_data: Dictionary = {}

## Determines whether the [StateMachine] starts when it gets added to the [SceneTree].
@export var start_on_ready: bool = true

var _current_state: State = null:
	set(new_state):
		_current_state = new_state
		@warning_ignore("shadowed_variable")
		var has_state = _current_state != null
		set_process(has_state)
		set_physics_process(has_state)
		set_process_input(has_state)
		set_process_unhandled_input(has_state)


# ---------------
# Virtual methods
# ---------------

func _init(states: Array[State] = []) -> void:
	add_states(states)


func _ready() -> void:
	@warning_ignore("shadowed_variable")
	var has_state = _current_state != null
	set_process(has_state)
	set_physics_process(has_state)
	set_process_input(has_state)
	set_process_unhandled_input(has_state)

	if start_on_ready:
		start()


func _process(delta: float) -> void:
	_current_state._process(delta)


func _physics_process(delta: float) -> void:
	_current_state._physics_process(delta)


func _input(event: InputEvent) -> void:
	_current_state._input(event)


func _unhandled_input(event: InputEvent) -> void:
	_current_state._unhandled_input(event)


# -------
# Methods
# -------

## Starts the [StateMachine]
func start() -> void:
	if _current_state == null and len(_states) > 0:
		_current_state = _states[0]

	for state in _states:
		_prepare_state(state)
		if not state._is_ready and is_inside_tree():
			state._ready()
			state._is_ready = true
	_current_state._enter({})


## Stops the [StateMachine]
func stop() -> void:
	set_process(false)
	set_physics_process(false)
	set_process_input(false)
	set_process_unhandled_input(false)


## Returns the current state
func get_current_state() -> State:
	return _current_state


## Returns the current states in an [Array]
func get_states() -> Array[State]:
	return _states


## Returns the state with the given name, if there isn't one, then returns with [code]null[/code]
func get_state(state_name: StringName) -> State:
	for state in _states:
		if state.name == state_name:
			return state
	return null


## Checks if there is a state with the given name
func has_state(state_name: StringName) -> bool:
	for state in _states:
		if state.name == state_name:
			return true
	return false


## Adds a state.[br]
## [br]
## Code example:
## [codeblock]
## var state_machine := StateMachine.new()
##
## state_machine.add_state(State.new())
## [/codeblock]
func add_state(state: State) -> void:
	_states.push_back(state)
	_prepare_state(state)


## Adds multiple states.[br]
## [br]
## Code example:
## [codeblock]
## var state_machine := StateMachine.new()
##
## state_machine.add_states([
##     State.new(),
##     State.new(),
## ])
## [/codeblock]
func add_states(states: Array[State]) -> void:
	self._states.append_array(states)
	for state in states:
		_prepare_state(state)


## Removes a [State] with a given name.
func remove_state(state_name: StringName) -> void:
	for i in range(len(_states)):
		var state: State = _states[i]
		if state.name == state_name:
			_unprepare_state(state)
			remove_state_at(i)
			break


## Removes multiple [State]s with the given names.
func remove_states(state_names: Array[StringName]) -> void:
	var indexes: Array[int] = []
	for i in range(len(_states)):
		var state: State = _states[i]
		if state.name in state_names:
			indexes.push_front(i)

	for i in indexes:
		remove_state_at(i)


## Removes a [State] at a given index.
func remove_state_at(i: int) -> void:
	var state: State = _states[i]
	_unprepare_state(state)
	_states.remove_at(i)


## Changes the current [State].
func change_state_to(new_state_name: StringName, data: Dictionary = {}) -> void:
	var next_state: State = null

	for state in _states:
		if state.name ==  new_state_name:
			next_state = state

	if next_state:
		if _current_state:
			_current_state._exit(data)
		_current_state = next_state
		_current_state._enter(data)


func _prepare_state(state: State) -> void:
	if not state._request_new_state.is_connected(change_state_to):
		state._request_new_state.connect(change_state_to)
	if state.name.is_empty():
		state.name = state.get_class()
	shared_data.merge(state.shared_data)
	state.shared_data = shared_data

	if not state._is_ready and is_inside_tree():
		state._ready()
		state._is_ready = true


func _unprepare_state(state: State) -> void:
	if _current_state.name == state.name:
		self._current_state = null
	state.request_new_state.disconnect(change_state_to)
