class_name State extends Resource
## A state for [StateMachine]


# ----------
# Properties
# ----------

## Name of the [State], used for referring to the [State] in other places.
## [br]
## Be sure to set a name in the constructor, or in the _ready function.
var name: StringName = ""

## A [Dictionary] for holding information between the [State]s
var shared_data: Dictionary = {}

@warning_ignore("unused_private_class_variable")
var _is_ready: bool = false


# -------
# Signals
# -------

signal _request_new_state(new_state_name: StringName, data: Dictionary)


# ---------------
# Virtual methods
# ---------------

@warning_ignore("shadowed_variable")
func _init(name: StringName = "") -> void:
	if not name.is_empty():
		self.name = name

## Called when the [StateMachine] gets added to the [SceneTree]
func _ready() -> void: pass

## Called during the processing step of the main loop.
@warning_ignore("unused_parameter")
func _process(delta: float) -> void: pass

## Called during the physics processing step of the main loop.
@warning_ignore("unused_parameter")
func _physics_process(delta: float) -> void: pass

## Called when there is an input event.
@warning_ignore("unused_parameter")
func _input(event: InputEvent) -> void: pass

## Called when an InputEvent hasn't been consumed by _input() or any GUI [Control] item.
@warning_ignore("unused_parameter")
func _unhandled_input(event: InputEvent) -> void: pass

## Called when an [InputEventKey] hasn't been consumed by _input() or any GUI [Control] item.
@warning_ignore("unused_parameter")
func _unhandled_key_input(event: InputEventKey) -> void: pass

## Called when the [State] changes to being active
@warning_ignore("unused_parameter")
func _enter(data: Dictionary) -> void: pass

## Called when the [State] changes to being inactive
@warning_ignore("unused_parameter")
func _exit(data: Dictionary) -> void: pass


# -------
# Methods
# -------

## Changes the owner [StateMachine]'s current state
func change_state_to(new_state_name: StringName, data: Dictionary = {}) -> void:
	_request_new_state.emit(new_state_name, data)
