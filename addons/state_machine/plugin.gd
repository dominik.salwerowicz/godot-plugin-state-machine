@tool
extends EditorPlugin


const plugin_name: String = "State Machine Plugin"
var _is_debug: bool = false


func _enter_tree() -> void:
	message("Loading...")
	message("Loaded")


func _exit_tree() -> void:
	message("Unloading...")
	message("Unloaded")


func message(text: String) -> void:
	if _is_debug:
		return
	print("%s: %s" % [plugin_name, text])
