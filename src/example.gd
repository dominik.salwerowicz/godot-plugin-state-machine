extends Node


func _ready() -> void:
	var state_machine := StateMachine.new([
		WalkingState.new("Walking"),
		FlyingState.new("Flying"),
	])

	state_machine.shared_data["player"] = $%"Player"

	add_child(state_machine)


class WalkingState extends State:
	var player: CharacterBody2D


	func _ready():
		player = shared_data.get("player")


	func _enter(_data) -> void:
		var state_label: Label = player.get_node("StateLabel")
		state_label.text = "Walking"


	func _physics_process(_delta):
		var dir: float = Input.get_axis("ui_left", "ui_right")
		player.velocity.x = dir * 300

		if player.is_on_floor():
			if Input.is_action_just_pressed("ui_up"):
				player.velocity.y = -1000
			else:
				player.velocity.y = 50
		else:
			player.velocity.y += 50
		player.move_and_slide()

		# Return the player, if it got too far
		if player.global_position.distance_squared_to(Vector2()) > 500_000:
			player.global_position = Vector2()

		if Input.is_action_just_pressed("ui_accept"):
			change_state_to("Flying")


class FlyingState extends State:
	var player: CharacterBody2D


	func _ready():
		player = shared_data.get("player")


	func _enter(_data) -> void:
		var state_label: Label = player.get_node("StateLabel")
		state_label.text = "Flying"


	func _physics_process(_delta):
		var dir = Input.get_vector("ui_left", "ui_right", "ui_up", "ui_down")
		player.velocity = dir * 300
		player.move_and_slide()

		# Return the player, if it got too far
		if player.global_position.distance_squared_to(Vector2()) > 500_000:
			player.global_position = Vector2()

		if Input.is_action_just_pressed("ui_accept"):
			change_state_to("Walking")
